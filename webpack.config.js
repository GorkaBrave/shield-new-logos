const webpack = require('webpack');
const path = require('path');

const config = {
  entry: {
    entry: path.join(__dirname, "src/index.js")
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.min.js',
    libraryTarget: 'umd',
    library: 'veriswiss'
  },
  node: {
    net: 'empty',
    fs: 'empty',
    tls: 'empty'
  },
  // devtool: 'eval-source-map',
  module: {
    rules: [{
      test: /JSONStream/,
      use: 'shebang-loader'
    }],
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['env', 'react'],
          plugins: ['transform-class-properties']
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {loader: "style-loader"},
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: '[local]'
            }
          }
        ]
      }
    ]
  },
  plugins: [],
  resolve: {
    extensions: ['.js', '.jsx']
  }
};

// webpack production config.
if (process.env.NODE_ENV === 'production') {

  config.externals = {
    'react': 'react',
    'react-dom': 'react-dom'
  };

  config.plugins = [
    new webpack.DefinePlugin({ // <-- key to reducing React's size
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true
      },
      compress: {
        warnings: false,
        screw_ie8: true
      },
      comments: false
    }),
    new webpack.optimize.AggressiveMergingPlugin()//Merge chunks
  ];
}

module.exports = config;
