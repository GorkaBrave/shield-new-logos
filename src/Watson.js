import WebService from './WebService';


export default class Watson {

  constructor() {
    this.webService = new WebService();
  }

  static detectFacesURL() {
    const baseURL = 'https://nodejs-microservice-with-expressjs-altrb.eu-gb.mybluemix.net/';
    const endPoint = 'api/classify';

    return baseURL + endPoint;
  }

  /**
   * {"images":[{"classifiers":[{"classifier_id":"default","name":"default","classes":[{"class":"people","score":0.626,"type_hierarchy":"/person/person/people"},{"class":"person","score":0.854},{"class":"people (face)","score":0.507,"type_hierarchy":"/person/person/people (face)"},{"class":"person portrait photo","score":0.5,"type_hierarchy":"/person/person/person portrait photo"},{"class":"ultramarine color","score":0.733},{"class":"coal black color","score":0.664}]},{"classifier_id":"food","name":"food","classes":[{"class":"non-food","score":0.999}]}],"image":"75d09e00-b918-11e8-a203-0ba36a034116.jpg","faces":[{"age":{"min":49,"max":53,"score":0.65755516},"face_location":{"height":163,"width":119,"left":107,"top":86},"gender":{"gender":"MALE","score":0.99991405}}]}],"images_processed":1,"custom_classes":0,"raw":{"classify":"%7B%22images%22%3A%5B%7B%22classifiers%22%3A%5B%7B%22classifier_id%22%3A%22default%22%2C%22name%22%3A%22default%22%2C%22classes%22%3A%5B%7B%22class%22%3A%22people%22%2C%22score%22%3A0.626%2C%22type_hierarchy%22%3A%22%2Fperson%2Fperson%2Fpeople%22%7D%2C%7B%22class%22%3A%22person%22%2C%22score%22%3A0.854%7D%2C%7B%22class%22%3A%22people%20(face)%22%2C%22score%22%3A0.507%2C%22type_hierarchy%22%3A%22%2Fperson%2Fperson%2Fpeople%20(face)%22%7D%2C%7B%22class%22%3A%22person%20portrait%20photo%22%2C%22score%22%3A0.5%2C%22type_hierarchy%22%3A%22%2Fperson%2Fperson%2Fperson%20portrait%20photo%22%7D%2C%7B%22class%22%3A%22ultramarine%20color%22%2C%22score%22%3A0.733%7D%2C%7B%22class%22%3A%22coal%20black%20color%22%2C%22score%22%3A0.664%7D%5D%7D%2C%7B%22classifier_id%22%3A%22food%22%2C%22name%22%3A%22food%22%2C%22classes%22%3A%5B%7B%22class%22%3A%22non-food%22%2C%22score%22%3A0.999%7D%5D%7D%5D%2C%22image%22%3A%2275d09e00-b918-11e8-a203-0ba36a034116.jpg%22%2C%22faces%22%3A%5B%7B%22age%22%3A%7B%22min%22%3A49%2C%22max%22%3A53%2C%22score%22%3A0.65755516%7D%2C%22face_location%22%3A%7B%22height%22%3A163%2C%22width%22%3A119%2C%22left%22%3A107%2C%22top%22%3A86%7D%2C%22gender%22%3A%7B%22gender%22%3A%22MALE%22%2C%22score%22%3A0.99991405%7D%7D%5D%7D%5D%2C%22images_processed%22%3A1%2C%22custom_classes%22%3A0%2C%22raw%22%3A%7B%7D%7D","detectFaces":"%7B%22images%22%3A%5B%7B%22faces%22%3A%5B%7B%22age%22%3A%7B%22min%22%3A49%2C%22max%22%3A53%2C%22score%22%3A0.65755516%7D%2C%22face_location%22%3A%7B%22height%22%3A163%2C%22width%22%3A119%2C%22left%22%3A107%2C%22top%22%3A86%7D%2C%22gender%22%3A%7B%22gender%22%3A%22MALE%22%2C%22score%22%3A0.99991405%7D%7D%5D%2C%22image%22%3A%2275d09e00-b918-11e8-a203-0ba36a034116.jpg%22%7D%5D%2C%22images_processed%22%3A1%7D"}}
   */

  detectFaceInCanvas(canvas) {

    const url = Watson.detectFacesURL();
    return this.webService.detectFace(url, canvas.toDataURL("image/jpeg"))
        .then(function(response) {
          return response.json();
        })
        .then(function(json) {
          return json.images[0].faces[0];
        })

  }

  static getFirstFace(data) {
    if (!data || !data.images || data.images.length === 0) {
      return null;
    }

    const image = data.images[0];
    if (!image || !image.faces || image.faces === 0) {
      return null;
    }

    return data.faces[0];
  }

}