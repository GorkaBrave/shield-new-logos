import React, {Component} from 'react';
import './App.css';
import ReactCamera from "./ReactCamera";
import * as logger from "./log";
import Watson from "./Watson";

// Props
// String team: spain
// function complete(info)
class Selfie extends Component {

  static UserType = {
    undetermined: "undetermined", adult: "adult", minor: "minor", teenager: "teenager"
  };

  constructor(props) {
    super(props);

    this.reactCamera = React.createRef();
    this.watson = new Watson();

    this.processingCanvas = false;

    this.state = {
      watsonLoaded: false,
      capturingEnabled: false,
      personDetected: false,
      classificationDone: false,
      userType: Selfie.UserType.undetermined,
      detection: {
        faceDetected: false,
        faceFrontal: false,
        faceProportion: false,
        facePosition: false
      }
    };
  }

  componentDidMount() {
    // This has stopped working...
    // this.watson.getModuleToken(this.tokenCallback);
  }

  render() {
    return (
        <section className={"selfie-container"}>
        <div className={"logo-container"}>
          <img src={require('./logo.png')} />
        </div>
          <ReactCamera ref={this.reactCamera}
                       cameraSupportedCallback={this.cameraSupportedCallback}
                       capturingEnabled={this.state.capturingEnabled}
                       videoFrameCanvas={this.videoFrameCanvas}/>

          <div className={'lock-container' + this.animationLockClass()}>
            <div id={"lock"} className={'lock-top-part' + this.animationUnlockClass()}/>
            <div className={"lock-bottom-part"}/>
          </div>
          <div style={{fontSize: '16px', textAlign: 'center', height: '30px'}}>
                { !this.state.detection.faceDetected ? <div className={`look-camera-${this.state.personDetected ? 'shown' : 'hidden'}`}>
            Please, look straight<br/>
            into the camera
            </div> : null }
          </div>

        </section>
    );
  };

  cameraSupportedCallback = (supported) => {
    if (supported) {
      console.log("Camera supported");
      this.setState({capturingEnabled: true})
    }
  };

  animationLockClass() {
    return this.state.userType === Selfie.UserType.minor ? ' shake' : '';
  }

  animationUnlockClass() {
    if (this.state.userType !== Selfie.UserType.adult) return '';

    let lock = document.getElementById('lock');
    lock.addEventListener('animationend', () => {
      this.finishComponent({})
    });

    return ' unlock';
  }

  videoFrameCanvas = (canvas) => {
    if (this.state.personDetected) {
      logger.log("Person already detected");
      return;
    }

    this.canvas = canvas;
    if (!this.processingCanvas) {
      this.processingCanvas = true;
      this.estimateSinglePose();
      this.processingCanvas = false;
    }
  };

  async estimateSinglePose() {
    const flipHorizontal = true;
    const outputStride = 32;
    const scaleFactor = 0.50;

    const pose = await this.props.posenet.estimateSinglePose(this.canvas, scaleFactor, flipHorizontal, outputStride);

    this.processPose(pose);
  }

  processPose(pose) {
    const faceElementsKeys = ['leftEye', 'rightEye', 'nose'];
    const faceKeyPoints = this.filterFaceKeyPoints(pose, faceElementsKeys);
    //this.paintFaceKeyPoints(faceKeyPoints);

    if (faceKeyPoints.length !== faceElementsKeys.length) {
      logger.log("Only " + faceKeyPoints.length + " of " + faceElementsKeys.length + " detected.");
      return;
    }

    const detection = this.state.detection;
    detection.faceDetected = true;

    // following code needs to be replaced with an object detector (bounding box face)
    // calculation depends on distance between eyes and nose (can vary greatly across people)
    const rightEye_y = faceKeyPoints.find(element => element.part === 'rightEye').position.y;
    const mouth_y = faceKeyPoints.find(element => element.part === 'nose').position.y;

    // first check the proportion of the head inside the canvas as it impacts the tilting threshold
    // face proportion in image OK?
    detection.faceProportion = (mouth_y - rightEye_y) >= this.toCanvasCoordY(8);
    detection.facePosition = rightEye_y <= this.toCanvasCoordY(55) && rightEye_y >= this.toCanvasCoordY(45);

    // check if frontal image (head not tilted left nor right)
    const nose_x = faceKeyPoints.find(element => element.part === 'nose').position.x;
    const leftEye_x = faceKeyPoints.find(element => element.part === 'leftEye').position.x;
    const rightEye_x = faceKeyPoints.find(element => element.part === 'rightEye').position.x;

    const d_1 = leftEye_x - nose_x;
    const d_2 = nose_x - rightEye_x;

    // TODO: tilting param is linearly related to face scaling
    detection.faceFrontal = Math.abs(d_2 - d_1) <= this.toCanvasCoordX(4);

    logger.log('faceProportion: ' + detection.faceProportion + ' facePosition: ' + detection.facePosition + ' faceFrontal: ' + detection.faceFrontal);

    const personDetected = Selfie.isPersonDetected(detection);
    this.setState({
      detection: detection,
      personDetected: personDetected,
      capturingEnabled: !personDetected,
    });

    if (personDetected && !this.state.classificationDone) {
      this.sendCanvasToWatson();
      this.setState({classificationDone: true});
    }
  }

  filterFaceKeyPoints(pose, faceElementsKeys) {
    const minimumAcceptableScore = 0.4;
    return pose.keypoints.filter(keypoint => {
      // Pass minimum score filter and it is a face keypoint
      if (faceElementsKeys.indexOf(keypoint.part) === -1) {
        return false;
      }
      return keypoint.score >= minimumAcceptableScore;
    });
  }

  sendCanvasToWatson() {
    this.watson.detectFaceInCanvas(this.canvas).then(result=>{
      this.checkUserType(result);
    })
  }

  static isPersonDetected(detection) {
    return detection.faceDetected;
  }

checkUserType(face) {
    const age = face.age;
    const userType = Selfie.userAgeType(age);
    this.setState({userType: userType});

    console.log("user " + userType + " age: " + age.min + "-" + age.max + " score: " + age.score.toFixed(2));
  }

  // Returms UserType
  static userAgeType(age) {

    if (age.score < 0.2) {
      return Selfie.UserType.undetermined;
    }

    if (age.min >= 22) {
      return Selfie.UserType.adult;
    }

    if (age.max <= 16) {
      return Selfie.UserType.minor;
    }

    return Selfie.UserType.teenager;
  }

  toCanvasCoordX(coord) {
    const canvasBaseWidth = 101;
    return (coord / canvasBaseWidth) * this.canvas.width;
  }

  toCanvasCoordY(coord) {
    const canvasBaseHeight = 101;
    return coord / canvasBaseHeight * this.canvas.height;
  }

  tokenCallback = (result) => {
    const {data, error} = result;
    const watsonFailed = error || !data;
    this.setState({watsonLoaded: !watsonFailed});
    if (watsonFailed) {
      logger.warn("error " + error);
    }
  };

  finishComponent = (result) => {
    if (result.error != null) {
      logger.error("error " + result.error);
    }
    this.props.complete(this.state.userType);
  };


}

export default Selfie;
