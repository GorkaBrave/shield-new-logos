import React, {Component} from 'react';
import ChatBot from 'react-simple-chatbot';
import { ThemeProvider } from 'styled-components';
import PropTypes from 'prop-types';
import Snapshot from './Snapshot';
import ChatbotHeader from './ChatbotHeader';

// Documentation: https://lucasbassetti.com.br/react-simple-chatbot/#/docs/custom

export default class Chatbot extends Component {

    constructor(props){

        super(props);
        this.reactCamera = React.createRef();

        this.state = {
            loading: true,
            result: '',
            trigger: false,
            width: 0,
            height: 0
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

        this.flow = {
            steps: [
            {
                id: '0',
                message: "Hi, I am your Veriswiss assistant! How can I help you? \n Are you older than 18 years?",
                trigger: '2',
            },
            {
                id: '1',
                message: "Our AI system thinks you're younger than 18 years, are you?",
                trigger: '2',
            },
            {
                id: '2',
                options: [
                    { value: '0', label: 'No', trigger: '5' },
                    { value: '1', label: 'Yes', trigger: '3' }
                ]
            },
            {
                id: '3',
                message: "We can verify your age based on one of these documents",
                trigger: '4',
            },
            {
                id: '4',
                options: [
                    { value: '1', label: 'Passport', trigger: '6' },
                    { value: '2', label: "Driver's license", trigger: '7' }
                ]
            },
            {
                id: '5',
                message: "This site is only accessible for 18+",
                end: true
            },
            {
                id: '6',
                message: "Please take a picture of your document, we're not storing any information, we're just checking your age",
                trigger: '8'
            },
            {
                id: '7',
                message: "Please take a picture of document, we're not storing any information, we're just checking your age",
                end: true
            },
            {
                id: '8',
                waitAction: true,
                trigger: '9',
                component: <Snapshot />,
            },
            {
                id: '9',
                message: "Awesome! Your age is confirmed. If you prefer you can register to our veriswiss service so you won't have to go through this again.",
                trigger: '12'
            },
            {
                id: '10',
                message: "Congratulations, you can now anonymously use our veriswiss verification service",
                end: true
            },
            {
                id: '11',
                    options: [
                    { value: 'yes', label: 'Yes', trigger: '10' },
                    { value: 'no', label: 'No', trigger: '5' }
                ]
            },
            {
                id: '12',
                options: [
                    { value: 'accept', label: 'Accept', trigger: '10' },
                    { value: 'decline', label: 'Decline', trigger: '5' }
                ]
            }
        ]};

    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {

        const otherFontTheme = {
            background: '#f5f8fb',
            fontFamily: 'Helvetica Neue',
            headerBgColor: '#6e48aa',
            headerFontColor: '#fff',
            headerFontSize: '16px',
            botBubbleColor: '#565150',
            botFontColor: '#fff',
            userBubbleColor: '#fff',
            userFontColor: '#4a4a4a',
        };
        return (
            <ThemeProvider theme={otherFontTheme}>
                <ChatBot
                    botAvatar={require('./light.jpg')}
                    headerComponent={<ChatbotHeader />}
                    headerTitle="Veriswiss Assistant"
                    style={{borderRadius:'0px', height: this.state.height}}
                    steps={this.flow.steps} />
            </ThemeProvider>
        );
    };

}

Chatbot.propTypes = {
    steps: PropTypes.object,
    triggerNextStep: PropTypes.func,
};

Chatbot.defaultProps = {
    steps: undefined,
    triggerNextStep: undefined,
};