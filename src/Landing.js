import React, {Component} from 'react';
import { FaCommentAlt } from 'react-icons/fa';

// Props
// function complete(nextScreen: String)
class Landing extends Component {

  constructor(props){
    super(props)
    this.state = {
      width: 0,
      height: 0
    }
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }



  render() {
    return (
        <section className={"landing-container"} >
          <div className={"logo-container"}>
            <img src={require('./logo.svg')} />
          </div>
        <div style={{cursor: 'pointer',}}>
          {this.renderContinueButton()} </div>
          <div className={"learn-more"} style={{cursor: 'pointer',}} onClick={this.infoSelected}>
            Click here to learn more about how<br/>
            <strong>we protect your privacy</strong>
          </div>

          <div className={"learn-more"} >

          </div>
        <div style={{height: '40px',
      width: '40px',
        paddingTop: '11px',
        paddingLeft: '10px',
        cursor: 'pointer',
      backgroundColor: '#E2E2E2',
      borderRadius: '50%',
      display: 'inline-block'}}>
          <FaCommentAlt style={{ color: '#545454', fontSize: '20px' }} onClick={this.chatbotSelected} />
        </div>
        </section>
    );
  }

  renderContinueButton() {
    const continueDisabled = this.props.canContinue ? '' : "disabled";
    const title = this.props.canContinue ? "continue" : "loading...";
    return (
        <div className={"button square " + continueDisabled}
             onClick={this.continueSelected}>{title}
        </div>
    );
  }

  infoSelected = () => {
    this.props.complete({selected: "info"});
  };

  chatbotSelected = () => {
    console.log('chatbot selected');
    this.props.complete({selected: "chatbot"});
  };

  continueSelected = () => {
    if (!this.props.canContinue) return;

    this.props.complete({selected: "continue"});
  };

}

export default Landing;
