import React, {Component} from 'react';
import './App.css'

import VideoCapture from './VideoCapture';
import * as logger from "./log";

// const dataURLtoBlob = require('blueimp-canvas-to-blob');
// const loadImage = require('blueimp-load-image');

// Props
// function pictureTakenCanvas(canvas)
// function videoFrameCanvas(canvas)
// function capturingEnabled(bool)
class ReactCamera extends Component {

  steps = {
    camera: "camera",
    pictureTaken: "pictureTaken"
  };

  constructor(props) {
    super(props);

    this.videoCapture = React.createRef();

    this.state = {
      loading: true,
      faceCamera: true, // Camera selfie or back camera
      cameraSupported: true,
      step: this.steps.camera
    };
  }

  resetCamera = () => {
    this.setState({step: this.steps.camera, loading: false})
  };

  render() {
    return (
        <div className="camera-container">
          {this.showCameraOrPicture()}
        </div>
    );
  }

  showCameraOrPicture = () => {
    if (this.state.step === this.steps.camera) {

      if (!this.state.cameraSupported) {
        logger.log("Error camera not supported");
        return null;
      }
      return this.renderVideoCapture();

    } else if (this.state.step === this.steps.pictureTaken) {
      return (
          <div className={'picture-frame round-border'}>
            {this.renderPictureTaken()}
          </div>
      );
    }
  };

  renderVideoCapture = () => {
    // Show faceMask if true or nil,
    const className = 'video-capture round-border ' + (this.state.faceCamera ? 'selfie' : '');
    return (
        <div className={"picture-frame"}>
          <VideoCapture className={className}
                        maxSize={224} face={this.state.faceCamera}
                        cameraSupportedCallback={this.cameraSupportedCallback}
                        cameraFrameCallback={this.cameraFrameCallback}
                        capturingEnabled={this.props.capturingEnabled}
                        ref={this.videoCapture}/>
        </div>
    );
  };

  cameraFrameCallback = (canvas) => {
    console.log("Canvas size is " + canvas.width + "x" + canvas.height);
    this.props.videoFrameCanvas(canvas);
  };

  cameraSupportedCallback = (supported) => {
    if (!supported) {
      logger.warn('camera media not supported');
    }
    this.setState({cameraSupported: supported});
    this.props.cameraSupportedCallback(supported);
  };

  renderPictureTaken = () => {
    this.dataURL = this.canvas.toDataURL();
    // const blob = dataURLtoBlob(this.dataURL);
    // this.props.pictureTakenBlob(blob);
    this.props.pictureTakenCanvas(this.canvas);

    return (
        <div className={this.state.faceCamera ? 'selfie' : ''}>
          <img src={this.dataURL}
               alt="your selfie"/>
        </div>
    );
  };

}

export default ReactCamera;
