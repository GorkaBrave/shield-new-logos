import React, {Component} from 'react';
import ReactCamera from "./ReactCamera";
import PreloaderIcon from 'react-preloader-icon';
import Oval from 'react-preloader-icon/loaders/Oval';

export default class Snapshot extends Component {

  constructor(props) {
    super(props);
    this.state = {
      capturingEnabled: true,
      renderChild: 'camera'
    };
    this.triggetNext = this.triggetNext.bind(this);
  }

  render() {
    return (
        <div style={{textAlign: 'center'}}>

          {(() => {
            switch (this.state.renderChild) {
              case 'camera':
                return (
                    <div>
                      <ReactCamera ref={this.reactCamera}
                                   cameraSupportedCallback={this.cameraSupportedCallback}
                                   capturingEnabled={this.state.capturingEnabled}
                                   videoFrameCanvas={this.videoFrameCanvas}
                                   faceCamera={false}/>

                      <img src={require('./camera.png')} onClick={() => this.triggetNext()}/>

                    </div>);
              case 'preloader':
                return (<PreloaderIcon
                    loader={Oval}
                    size={32}
                    strokeWidth={8} // min: 1, max: 50
                    strokeColor="#F0AD4E"
                    duration={800}
                />);
              case 'result':
                return <div style={{fontSize: '14px'}}> We matched your image with the one on your ID card, and
                  extracted your date of birth </div>
              default :
                return null;
            }
          })()}

        </div>
    )
  }


  cameraSupportedCallback = (supported) => {
    if (supported) {
      console.log("Camera supported");
      this.setState({capturingEnabled: true})
    }
  };

  videoFrameCanvas = (canvas) => {
    this.canvas = canvas;
    this.processingCanvas = false;
  };

  triggetNext() {
    this.setState({trigger: true}, () => {
      this.setState({renderChild: 'preloader'});

      setTimeout(() => {
        this.props.triggerNextStep();
        this.setState({renderChild: 'result'});
      }, 1000);
    });
  }

}