import React, {Component} from 'react';
import './App.css'

import VideoCapture from './VideoCapture';
import * as logger from "./log";
import Selfie from './Selfie'

// Props
class IdCard extends Component {

  steps = {
    camera: "camera",
    pictureTaken: "pictureTaken"
  };


  constructor(props) {
    super(props);

    this.videoCapture = React.createRef();

    this.state = {
      faceCamera: false,
      cameraSupported: true,
      step: this.steps.camera
    };
  }

  render() {
    return (
        <section className={"id-camera-container"}>
          <div className={"logo-container"}>
            <div className={"logo"}/>
          </div>
          <div className="camera-container">
            {this.showCameraOrPicture()}
          </div>
          <div className={"look-camera"}>
            Please, put your id card<br/>
            in front of the camera
          </div>
          <div className={'lock-container' + this.animationLockClass()}>
            <div id={"lock"} className={'lock-top-part' + this.animationUnlockClass()}/>
            <div className={"lock-bottom-part"}/>
          </div>
        </section>
    );
  }

  animationLockClass() {
    return this.state.userType === Selfie.UserType.minor ? ' shake' : '';
  }

  animationUnlockClass() {
    if (this.state.userType !== Selfie.UserType.adult) return '';

    let lock = document.getElementById('lock');
    lock.addEventListener('animationend', () => {
      this.finishComponent({})
    });

    return ' unlock';
  }

  showCameraOrPicture = () => {
    if (this.state.step === this.steps.camera) {

      if (!this.state.cameraSupported) {
        logger.log("Error camera not supported");
        return null;
      }
      return this.renderVideoCapture();

    } else if (this.state.step === this.steps.pictureTaken) {
      return (
          <div className={'picture-frame round-border'}>
            {this.renderPictureTaken()}
          </div>
      );
    }
  };

  renderVideoCapture = () => {
    // Show faceMask if true or nil,
    const className = 'video-capture round-border ' + (this.state.faceCamera ? 'selfie' : '');
    return (
        <div className={"picture-frame"}>
          <VideoCapture className={className}
                        maxSize={224} face={this.state.faceCamera}
                        cameraSupportedCallback={this.cameraSupportedCallback}
                        cameraFrameCallback={this.cameraFrameCallback}
                        capturingEnabled={this.props.capturingEnabled}
                        ref={this.videoCapture}/>
        </div>
    );
  };

  cameraFrameCallback = (canvas) => {
    this.props.videoFrameCanvas(canvas);
  };

  cameraSupportedCallback = (supported) => {
    if (!supported) {
      logger.warn('camera media not supported');
    }
    this.setState({cameraSupported: supported});
  };

  takePicture = () => {
    if (this.state.step === this.steps.pictureTaken) {
      // After piture taken, press again to go to camera
      this.setState({step: this.steps.camera});
      return;
    }

    // Taking picture
    this.canvas = this.videoCapture.getScreenshot();
    if (this.canvas != null) {
      this.props.pictureTakenCanvas(this.canvas);
      // this.setState({step: this.steps.pictureTaken});
      // Callback with picture
    }
  };

  renderPictureTaken = () => {
    this.dataURL = this.canvas.toDataURL();
    // const blob = dataURLtoBlob(this.dataURL);
    // this.props.pictureTakenBlob(blob);
    this.props.pictureTakenCanvas(this.canvas);

    return (
        <div className={this.state.faceCamera ? 'selfie' : ''}>
          <img src={this.dataURL}
               alt="your selfie"/>
        </div>
    );
  };

}

export default IdCard;
